"""
USAGE: update_json <file>
"""
import docopt
import json
import sys

def main():
    args = docopt.docopt(__doc__)
    old = args["<file>"]
    ids = {}
    with open(old) as fp:
        data = json.load(fp)        
        for elt in data["data"]:  
            elt["isNew"] = False
            ids[elt["id"]] = elt

    newdata = json.load(sys.stdin)
    for elt in newdata["data"]:
        elt["isNew"] = elt["id"] not in ids

    print(json.dumps(newdata))

if __name__ == "__main__":
    main()
